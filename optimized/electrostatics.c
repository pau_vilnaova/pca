/*
This file is part of ftdock, a program for rigid-body protein-protein docking 
Copyright (C) 1997-2000 Gidon Moont

Biomolecular Modelling Laboratory
Imperial Cancer Research Fund
44 Lincoln's Inn Fields
London WC2A 3PX

+44 (0)20 7269 3348
http://www.bmm.icnet.uk/

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#include "structures.h"

void assign_charges( struct Structure This_Structure ) {

/************/

  /* Counters */

  int	residue , atom ;

/************/

  for( residue = 1 ; residue <= This_Structure.length ; residue ++ ) {
    for( atom = 1 ; atom <= This_Structure.Residue[residue].size ; atom ++ ) {

      This_Structure.Residue[residue].Atom[atom].charge = 0.0 ;

      /* peptide backbone */

      if( strcmp( This_Structure.Residue[residue].Atom[atom].atom_name , " N  " ) == 0 ) {
        if( strcmp( This_Structure.Residue[residue].res_name , "PRO" ) == 0 ) {
          This_Structure.Residue[residue].Atom[atom].charge = -0.10 ;
        } else {
          This_Structure.Residue[residue].Atom[atom].charge =  0.55 ;
          if( residue == 1 ) This_Structure.Residue[residue].Atom[atom].charge = 1.00 ;
        }
      }

      if( strcmp( This_Structure.Residue[residue].Atom[atom].atom_name , " O  " ) == 0 ) {
        This_Structure.Residue[residue].Atom[atom].charge = -0.55 ;
        if( residue == This_Structure.length  ) This_Structure.Residue[residue].Atom[atom].charge = -1.00 ;
      }

      /* charged residues */

      if( ( strcmp( This_Structure.Residue[residue].res_name , "ARG" ) == 0 ) && ( strncmp( This_Structure.Residue[residue].Atom[atom].atom_name , " NH" , 3 ) == 0 ) ) This_Structure.Residue[residue].Atom[atom].charge =  0.50 ;
      if( ( strcmp( This_Structure.Residue[residue].res_name , "ASP" ) == 0 ) && ( strncmp( This_Structure.Residue[residue].Atom[atom].atom_name , " OD" , 3 ) == 0 ) ) This_Structure.Residue[residue].Atom[atom].charge = -0.50 ;
      if( ( strcmp( This_Structure.Residue[residue].res_name , "GLU" ) == 0 ) && ( strncmp( This_Structure.Residue[residue].Atom[atom].atom_name , " OE" , 3 ) == 0 ) ) This_Structure.Residue[residue].Atom[atom].charge = -0.50 ;
      if( ( strcmp( This_Structure.Residue[residue].res_name , "LYS" ) == 0 ) && ( strcmp( This_Structure.Residue[residue].Atom[atom].atom_name , " NZ " ) == 0 ) ) This_Structure.Residue[residue].Atom[atom].charge =  1.00 ;

    }
  }

/************/

}



/************************/



void electric_field( struct Structure This_Structure , float grid_span , int grid_size , fftw_real *grid ) {

/************/
  /* Counters */

  int	residue , atom ;

  /* Co-ordinates */

  int	x , y , z ;
  float		x_centre , y_centre , z_centre ;

  /* Variables */

  float32_t		distance [4]__attribute__((aligned(16)));
  float32_t		 epsilon[4]__attribute__((aligned(16)));
	float32_t phi;
	

	/* intrinsicts */
	float32x4_t dist, epsi, mask2, mask3;
	float32_t values[4]__attribute__((aligned(16)));
/************/
  printf( "  electric field calculations ( one dot / grid sheet ) " ) ;
	int sumGrid = grid_size + 2;
	int yAddress;
	float centre[grid_size];
  float spanDivSize = (float)( (grid_span) / (float)(grid_size) );
	float halfSpan = (float)( (grid_span) / 2);
	float span2 = spanDivSize*spanDivSize;
  for( x = 0 ; x < grid_size ; x ++ ) {
		printf(".");
		centre[x] = ( (float)(x) + .5) * spanDivSize - halfSpan;
    for( y = 0 ; y < grid_size ; y ++ ) {
			yAddress = (y +  grid_size * x) * sumGrid;
      for( z = 0 ; z < grid_size ; z ++ ) {
        //grid[gaddress(x,y,z,grid_size)] = (fftw_real)0 ;
        grid[yAddress+z] = (fftw_real)0 ;				
      }
    }
  }

/************/

  setvbuf( stdout , (char *)NULL , _IONBF , 0 ) ;

  //printf( "  electric field calculations ( one dot / grid sheet ) " ) ;
	
  //float spanDivSize = (float)( (grid_span) / (float)(grid_size) );
	//float halfSpan = (float)( (grid_span) / 2);
	float coord1, coord2, coord3, x_p, y_p, z_p;
	float z_p2, y_p2, x_p2,x_y, x_y_z;
	float charge_a;
	
	for (residue = 1; residue <= This_Structure.length; ++residue) {
		for (atom = 1; atom <= This_Structure.Residue[residue].size; ++atom) {
			if (This_Structure.Residue[residue].Atom[atom].charge != 0) {
				coord1 = This_Structure.Residue[residue].Atom[atom].coord[1];
				coord2 = This_Structure.Residue[residue].Atom[atom].coord[2];
				coord3 = This_Structure.Residue[residue].Atom[atom].coord[3];
				charge_a = This_Structure.Residue[residue].Atom[atom].charge;
				
					x_p = coord1 - centre[0];
					x_p2 = x_p * x_p;
				for (x = 0; x < grid_size; ++x) {
					y_p = coord2 - centre[0];
					y_p2 = y_p * y_p;
					for (y = 0; y < grid_size; ++y) {
						yAddress = (y +  grid_size * x) * sumGrid;
						z_p = coord3 - centre[0];
						z_p2 = z_p* z_p;
						x_y = y_p2 + x_p2;
						for (z = 0; z < grid_size-3; z+=4) {
							values[0] = x_y + z_p2;
							z_p2 = z_p2 - 2*z_p*spanDivSize + span2; //(z_p anterior + spandivsize)² = z_p anterior ² +  2*z_p anterior * spandivsize + spandivsize²
              z_p -= spanDivSize;
							values[1] = x_y + z_p2;
							z_p2 = z_p2 - 2*z_p*spanDivSize + span2; //(z_p anterior + spandivsize)² = z_p anterior ² +  2*z_p anterior * spandivsize + spandivsize²
              z_p -= spanDivSize;
							values[2] = x_y + z_p2;
							z_p2 = z_p2 - 2*z_p*spanDivSize + span2; //(z_p anterior + spandivsize)² = z_p anterior ² +  2*z_p anterior * spandivsize + spandivsize²
              z_p -= spanDivSize;
							values[3] = x_y + z_p2;
							z_p2 = z_p2 - 2*z_p*spanDivSize + span2; //(z_p anterior + spandivsize)² = z_p anterior ² +  2*z_p anterior * spandivsize + spandivsize²
              z_p -= spanDivSize;
							//dist = vld1q_f32(values);

							distance[0] = sqrt(values[0]);
							epsilon[0] = 4;
							if (distance[0] < 2.0) distance[0] = 2.0;
							else if (distance[0] >= 8.0) epsilon[0] = 80;
							else if (distance[0] > 6.0) epsilon[0] = (38*distance[0]) - 224;


							distance[1] = sqrt(values[1]);
							epsilon[1] = 4;
							if (distance[1] < 2.0) distance[1] = 2.0;
							else if (distance[1] >= 8.0) epsilon[1] = 80;
							else if (distance[1] > 6.0) epsilon[1] = (38*distance[1]) - 224;

							distance[2] = sqrt(values[2]);
							epsilon[2] = 4;
							if (distance[2] < 2.0) distance[2] = 2.0;
							else if (distance[2] >= 8.0) epsilon[2] = 80;
							else if (distance[2] > 6.0) epsilon[2] = (38*distance[2]) - 224;

							distance[3] = sqrt(values[3]);
							epsilon[3] = 4;
							if (distance[3] < 2.0) distance[3] = 2.0;
							else if (distance[3] >= 8.0) epsilon[3] = 80;
							else if (distance[3] > 6.0) epsilon[3] = (38*distance[3]) - 224;
							/*dist = vld1q_f32(distance);
							epsi = vld1q_f32(epsilon);
							vst1q_f32(distance,vmulq_f32(epsi,dist));*/
              grid[yAddress+z] += (fftw_real) ( charge_a /(epsilon[0] *  distance[0])) ;
              grid[yAddress+z+1] += (fftw_real) ( charge_a / (epsilon[1] *  distance[1] )) ;
              grid[yAddress+z+2] += (fftw_real) ( charge_a /  (epsilon [2] * distance[2] ) ) ;
              grid[yAddress+z+3] += (fftw_real) ( charge_a /  (epsilon[3] * distance[3] ) ) ;
              /*grid[yAddress+z] += (fftw_real) ( charge_a /( distance[0])) ;
              grid[yAddress+z+1] += (fftw_real) ( charge_a / (distance[1] )) ;
              grid[yAddress+z+2] += (fftw_real) ( charge_a /  (distance[2] ) ) ;
              grid[yAddress+z+3] += (fftw_real) ( charge_a /  (distance[3] ) ) ;*/

						}
						
						for (; z < grid_size; z++) {
							x_y_z = x_y + z_p2;
							distance[0] = sqrt(x_y_z);
							epsilon[0] = 4;
							if (distance[0] < 2.0) distance[0] = 2.0;
							else if (distance[0] >= 8.0) epsilon[0] = 80;
							else if (distance[0] > 6.0) epsilon[0] = (38*distance[0]) - 224;
              grid[yAddress+z] += (fftw_real) ( charge_a / ( epsilon[0] * distance[0] ) ) ;

							z_p2 = z_p2 - 2*z_p*spanDivSize + span2; //(z_p anterior + spandivsize)² = z_p anterior ² +  2*z_p anterior * spandivsize + spandivsize²
              z_p -= spanDivSize;											//(2 + 0.5) ² = 4 + 2*2*0.5 + 0.5²
						}
						y_p2 = y_p2 - 2*y_p*spanDivSize + span2;
						y_p -= spanDivSize;
					}
					x_p2 = x_p2 - 2*x_p*spanDivSize + span2;
					x_p -=spanDivSize;
				}
			}
		}
	}

  /*for( x = 0 ; x < grid_size ; x ++ ) {

    printf( "." ) ;

    //x_centre  = gcentre( x , grid_span , grid_size ) ;
		x_centre = ( (float)(x) + .5) * spanDivSize - halfSpan;  

    for( y = 0 ; y < grid_size ; y ++ ) {

      //y_centre  = gcentre( y , grid_span , grid_size ) ;
		  y_centre = ( (float)(y) + .5) * spanDivSize - halfSpan;  
			yAddress = (y +  grid_size * x) * sumGrid;

      for( z = 0 ; z < grid_size ; z ++ ) {

        //z_centre  = gcentre( z , grid_span , grid_size ) ;
				z_centre = ( (float)(z) + .5) * spanDivSize - halfSpan;  

        phi = 0 ;

        for( residue = 1 ; residue <= This_Structure.length ; residue ++ ) {
          for( atom = 1 ; atom <= This_Structure.Residue[residue].size ; atom ++ ) {

            if( This_Structure.Residue[residue].Atom[atom].charge != 0 ) {

             //distance = pythagoras( This_Structure.Residue[residue].Atom[atom].coord[1] , This_Structure.Residue[residue].Atom[atom].coord[2] , This_Structure.Residue[residue].Atom[atom].coord[3] , x_centre , y_centre , z_centre ) ;
              float x_p = This_Structure.Residue[residue].Atom[atom].coord[1] - x_centre;
              float y_p = This_Structure.Residue[residue].Atom[atom].coord[2] - y_centre;
              float z_p = This_Structure.Residue[residue].Atom[atom].coord[3] - z_centre;
              distance = sqrt((x_p*x_p)+ (y_p * y_p) + (z_p * z_p));
	      epsilon = 4 ;

              if( distance < 2.0 ){
		distance = 2.0 ;
	      }
              
	      else if( distance >= 8.0 ) {

                epsilon = 80 ;

              } 
	      else if( distance > 6.0 ) { 

                epsilon = ( 38 * distance ) - 224 ;
              } 
              phi += ( This_Structure.Residue[residue].Atom[atom].charge / ( epsilon * distance ) ) ;

            }

          }
        }

        //grid[gaddress(x,y,z,grid_size)] = (fftw_real)phi ;
        grid[yAddress+z] = (fftw_real)phi ;

      }
    }
  }*/

  printf( "\n" ) ;

/************/
  return ;

}



/************************/



void electric_point_charge( struct Structure This_Structure , float grid_span , int grid_size , fftw_real *grid ) {

/************/

  /* Counters */

  int	residue , atom ;

  /* Co-ordinates */

  int	x , y , z ;
  int	x_low , x_high , y_low , y_high , z_low , z_high ;

  float		a , b , c ;
  float		x_corner , y_corner , z_corner ;
  float		w ;

  /* Variables */

  float		one_span ;

/************/

  for( x = 0 ; x < grid_size ; x ++ ) {
    for( y = 0 ; y < grid_size ; y ++ ) {
      for( z = 0 ; z < grid_size ; z ++ ) {

        grid[gaddress(x,y,z,grid_size)] = (fftw_real)0 ;

      }
    }
  }

/************/

  one_span = grid_span / (float)grid_size ;

  for( residue = 1 ; residue <= This_Structure.length ; residue ++ ) {
    for( atom = 1 ; atom <= This_Structure.Residue[residue].size ; atom ++ ) {

      if( This_Structure.Residue[residue].Atom[atom].charge != 0 ) {

        x_low = gord( This_Structure.Residue[residue].Atom[atom].coord[1] - ( one_span / 2 ) , grid_span , grid_size ) ;
        y_low = gord( This_Structure.Residue[residue].Atom[atom].coord[2] - ( one_span / 2 ) , grid_span , grid_size ) ;
        z_low = gord( This_Structure.Residue[residue].Atom[atom].coord[3] - ( one_span / 2 ) , grid_span , grid_size ) ;

        x_high = x_low + 1 ;
        y_high = y_low + 1 ;
        z_high = z_low + 1 ;

        a = This_Structure.Residue[residue].Atom[atom].coord[1] - gcentre( x_low , grid_span , grid_size ) - ( one_span / 2 ) ;
        b = This_Structure.Residue[residue].Atom[atom].coord[2] - gcentre( y_low , grid_span , grid_size ) - ( one_span / 2 ) ;
        c = This_Structure.Residue[residue].Atom[atom].coord[3] - gcentre( z_low , grid_span , grid_size ) - ( one_span / 2 ) ;

        for( x = x_low ; x <= x_high  ; x ++ ) {
 
          x_corner = one_span * ( (float)( x - x_high ) + .5 ) ;

          for( y = y_low ; y <= y_high  ; y ++ ) {

            y_corner = one_span * ( (float)( y - y_high ) + .5 ) ;

            for( z = z_low ; z <= z_high  ; z ++ ) {

              z_corner = one_span * ( (float)( z - z_high ) + .5 ) ;

              w = ( ( x_corner + a ) * ( y_corner + b ) * ( z_corner + c ) ) / ( 8.0 * x_corner * y_corner * z_corner ) ;

              grid[gaddress(x,y,z,grid_size)] += (fftw_real)( w * This_Structure.Residue[residue].Atom[atom].charge ) ;

            }
          }
        }

      }

    }
  }

/************/

  return ;

}



/************************/



void electric_field_zero_core( int grid_size , fftw_real *elec_grid , fftw_real *surface_grid , float internal_value ) {

/************/

  /* Co-ordinates */

  int	x , y , z ;

/************/

  for( x = 0 ; x < grid_size ; x ++ ) {
    for( y = 0 ; y < grid_size ; y ++ ) {
      for( z = 0 ; z < grid_size ; z ++ ) {

        if( surface_grid[gaddress(x,y,z,grid_size)] == (fftw_real)internal_value ) elec_grid[gaddress(x,y,z,grid_size)] = (fftw_real)0 ;

      }
    }
  }

/************/

  return ;

}
