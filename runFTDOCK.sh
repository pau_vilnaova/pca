#!/bin/bash

fileExists () {
	if [ ! -f $1 ]
	then
		echo "${*:2}"
		exit 1
	fi
}

#Original
cd original
make > /dev/null 2>&1
msg='Couldnt compile the original program'
fileExists ftdock $msg

#Test1
echo "Test1 original"
./ftdock -static ../inputs/2pka.parsed -mobile ../inputs/5pti.parsed > original.test1
msg='Test1 original program didnt finish well'
fileExists timing.txt $msg
test1_org=$(cat timing.txt)
rm timing.txt

#Test2
echo "Test2 original"
./ftdock -static ../inputs/1ACB_rec.parsed -mobile ../inputs/1ACB_lig.parsed > original.test2
msg='Test2 original program didnt finish well'
fileExists timing.txt $msg
test2_org=$(cat timing.txt)
rm timing.txt

make clean > /dev/null 2>&1

#Optimized
cd ../optimized
make > /dev/null 2>&1
msg='Couldnt compile the optimized program'
fileExists ftdock $msg

#Test1
echo "Test1 optimized"
./ftdock -static ../inputs/2pka.parsed -mobile ../inputs/5pti.parsed > original.test1
msg='Test1 optimized program didnt finish well'
fileExists timing.txt $msg
test1_opt=$(cat timing.txt)
rm timing.txt

#Test2
echo "Test2 optimized"
./ftdock -static ../inputs/1ACB_rec.parsed -mobile ../inputs/1ACB_lig.parsed > original.test2
msg='Test2 optimized program didnt finish well'
fileExists timing.txt $msg
test2_opt=$(cat timing.txt)
rm timing.txt

make clean > /dev/null 2>&1

#Results
echo "Test1 timing:"
sp1=$(echo "scale=2;$test1_org/$test1_opt" | bc)
echo "	Original = $test1_org, Optimized = $test1_opt, SpeedUp = $sp1"
echo ""
echo "Test2 timing:"
sp2=$(echo "scale=2;$test2_org/$test2_opt" | bc)
echo "	Original = $test2_org, Optimized = $test2_opt, SpeedUp = $sp2"

